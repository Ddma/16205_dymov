#include "gtest\gtest.h"
#include "scheduler.h"

void open_and_read(std::fstream in)
{
	time_t now = time(NULL);
	scheduler sched(now);
	std::string str;
	std::getline(in, str);

	while (str.size() > 0) {
		EXPECT_THROW(parsing par(&sched, str), std::string);
		str.clear();
		std::getline(in, str);
	}
}

TEST(parsing_test, before_task_test) {
	std::fstream in("tests/before_task_test.txt", std::fstream::in);
	open_and_read(std::move(in));
	in.close();
}

TEST(parsing_test, task_test) {
	std::fstream in("tests/before_task_test.txt", std::fstream::in);
	open_and_read(std::move(in));
	in.close();
}

TEST(parsing_test, print_tasks_time_test) {
	std::fstream new_test("tests/print_tasks_time_test.txt", std::fstream::out);
	time_t now = time(NULL);
}

int main(int argc, char* argv[]) {
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
