#define _CRT_SECURE_NO_WARNINGS  //for function ctime

//todo
//make input file from console
//parcing check for directory/url


#include "scheduler.h"


///////////////////////////////////////////////////////////////
//global

std::time_t time_in(scheduler* sched, std::string str)
{
	std::vector<std::string> numb = split(str, ':');
	time_t sum = 0;//counting our time in seconds
	for (int i = 0; i < 3; ++i) {
		int num = atoi(numb[i].c_str());
		sum += (time_t)(num*pow(60, (2 - i)));
	}//sum how much time in time_t

	std::time_t ptime = sched->start_time();
	std::time_t date = ptime - (ptime % 86400);//

	tm*k = std::gmtime(&date);//making it to local time
	date = mktime(k);//for example we should get rid of 6 hours in Siberia as here (time_t = 0  <=> 06:00:00)

	sum += date;//needed time in our day


	if (sum <= sched->start_time()) {//if this time in our day already passed
		sum += 3600 * 24;//1 day
	}
	//can happen in some cases (if needed can describe in conversation)
	//anyways starting time should be lower than any other
	if (sum <= sched->start_time()) {
		sum += 3600 * 24;
	}
	return sum;
}


std::vector<std::string> split(std::string str, char delim)
{
	std::vector<std::string> words;
	std::stringstream ss(str);
	std::string word;
	while (std::getline(ss, word, delim))
	{
		words.push_back(word);
	}
	return words;
}


///////////////////////////////////////////////////////////////
//parsing

std::time_t parsing::get_time() const{
	return time;
}

std::string parsing::get_task() const{
	return task;
}

std::vector<std::string> parsing::get_param() const{
	return param;
}




bool parsing::check_time(std::string& str) const
{
	std::string error = "wrong time";
	if (!(str[2] == ':' && str[5] == ':'))
		throw error;
	std::string ft = "00:00:00";
	std::string st = "23:59:59";
	if (str >= ft && str <= st)
		return true;
	else
		return false;
}

bool parsing::check_directory(std::string& str) const
{
	std::fstream dir(str, std::fstream::in);
	std::string err = "incorrect directory";
	if (!dir.is_open())
		throw err;
	dir.close();
}

bool parsing::check_url(std::string& str) const{
	return true;
}



parsing::parsing(scheduler* sched, std::string& str) :this_sched(sched) {
	std::string error_string = "invalid string";
	std::string error_time = "incorrect time";
	std::vector<std::string> words;
	words = split(str, ' ');
	if (words.size() <= 2)
		throw error_string;
	if (check_time(words[0]))
		time = time_in(sched, words[0]);
	else
		throw error_time;
	task = words[1];
	words.erase(words.begin(), words.begin() + 2);
	//now check to parametrs of task should be correct
	if (task == "print_tasks") {
		if (words.size() != 2)
			throw error_string;
		if (!(check_time(words[0]) && check_time(words[1])) ||
			(time_in(sched, words[0]) > time_in(sched, words[1])))//checking time to be correct
			throw error_time;
	}
	else if (task == "play_music") {
		if (words.size() != 1)
			throw error_string;
		if (!check_directory(words[0])) {//checking direcroty to be correct
			std::string err_dir = "no such file or directory";
			throw err_dir;
		}
	}
	else if (task == "copy_file") {
		if (words.size() != 2)
			throw error_string;
		if (!(check_directory(words[0]) && check_directory(words[1]))) {//checking direcroty to be correct
			std::string err_dir = "no such file or directory";
			throw err_dir;
		}
	}
	else if (task == "show_message") {}
	else if (task == "open") {
		if (words.size() != 1)
			throw error_string;
		if (!check_url(words[0])) {//checking url to be correct
			std::string err_url = "incorrect url";
			throw err_url;
		}
	}
	else {
		std::string err_task = "dont have this type of task";
		throw err_task;
	}
	param = words;
}

///////////////////////////////////////////////////////////////
//tasks
print_tasks::print_tasks(scheduler* this_sched, time_t& from, time_t& to) : this_sched(this_sched), from(from), to(to) {}

std::string print_tasks::name() const{
	return "print_task";
}

void print_tasks::run() const 
{
	for (std::map < time_t, std::vector<task*> >::iterator it = this_sched->time_map.begin(); it != this_sched->time_map.end(); ++it) {
		if (it->first < from)
			continue;
		if (it->first > to)
			break;
		std::cout << "task/tasks at " << ctime(&it->first);
		for (int i = 0; i < it->second.size(); ++i)
			std::cout << it->second[i]->name() << std::endl;
	}
}



play_music::play_music(std::string& music) : music(music) {}

std::string play_music::name() const{
	return "play_music";
}

void play_music::run() const {
	ShellExecuteA(NULL, "open", music.c_str(), NULL, NULL, SW_SHOW);
}



copy_file::copy_file(std::string& from, std::string& to) : from(from), to(to) {}

std::string copy_file::name() const{
	return "copy_file";
}

void copy_file::run() const 
{
	std::fstream from_f(from.c_str(), std::fstream::in);
	std::fstream to_f(to.c_str(), std::fstream::out);
	char chr;
	while (from_f.get(chr))
		to_f << chr;
	from_f.close();
	to_f.close();
}



show_message::show_message(std::vector<std::string>& message) : message(message) {}

std::string show_message::name() const{
	return "show_message";
}

void show_message::run() const 
{
	std::string all_msg;
	for (int i = 0; i < message.size(); ++i) {
		all_msg += message[i];
		all_msg += " ";
	}
	MessageBoxA(NULL, all_msg.c_str(), "ok", MB_OK);
}



open::open(std::string& url) : url(url) {}

std::string open::name() const{
	return "open";
}

void open::run() const{
	ShellExecuteA(NULL, "open", url.c_str(), NULL, NULL, SW_SHOW);
}




///////////////////////////////////////////////////////////////
//scheduler
scheduler::scheduler(time_t& starting_time) : starting_time(starting_time) {}

std::time_t scheduler::start_time() {
	return starting_time;
}



scheduler::~scheduler() {
	for (std::map < time_t, std::vector<task*> >::iterator it = scheduler::time_map.begin(); it != scheduler::time_map.end(); ++it) {
		for (int i = 0; i < it->second.size(); ++i) {
			delete it->second[i];
		}
	}
}



void scheduler::push(const parsing& parsed)
{
	std::time_t time = parsed.get_time(); 
	std::string m_task = parsed.get_task();
	std::vector<std::string> param = parsed.get_param();
	task* ptask;
	if (m_task == "print_tasks") {
		time_t t1 = time_in(this, param[0]);
		time_t t2 = time_in(this, param[1]);
		ptask = new print_tasks(this, t1, t2);
		time_map[time].push_back(ptask);
	}
	else if (m_task == "play_music") {
		ptask = new play_music(param[0]);
		time_map[time].push_back(ptask);
	}
	else if (m_task == "copy_file") {
		ptask = new copy_file(param[0], param[1]);
		time_map[time].push_back(ptask);
	}
	else if (m_task == "show_message") {
		ptask = new show_message(param);
		time_map[time].push_back(ptask);
	}
	else if (m_task == "open") {
		ptask = new open(param[0]);
		time_map[time].push_back(ptask);
	}
}

void scheduler::start_work() {
	time_t basic_time = starting_time;
	for (std::map < time_t, std::vector<task*> >::iterator it = scheduler::time_map.begin(); it != scheduler::time_map.end(); ++it) {
		int diff = std::difftime(it->first, basic_time);
		std::chrono::seconds diff_time(diff);
		std::this_thread::sleep_for(diff_time);

		for (int i = 0; i < it->second.size(); ++i) {
			std::thread thr(std::thread(&task::run, it->second[i]));
			thread_list.push_back(std::move(thr));
		}
		basic_time = it->first;
	}
	join_all_threads();
}

void scheduler::join_all_threads() {
	for (std::list<std::thread>::iterator it = scheduler::thread_list.begin(); it != scheduler::thread_list.end(); ++it) {
		it->join();
	}
}

/*
int main()
{
	std::fstream in("input.txt", std::fstream::in);
	time_t my_time= time(NULL);
	scheduler schd(my_time);
	std::string str;
	std::getline(in, str);

	while (str.size() > 0) {
		try{
			parsing par(&schd, str);
			schd.push(par);
		}
		catch (std::string& err_str) {
			std::cout << err_str << std::endl;
		}
		str.clear();
		std::getline(in, str);
	}
	schd.start_work();
	in.close();
	return 0;
}
*/
