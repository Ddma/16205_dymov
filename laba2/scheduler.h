#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <chrono>
#include <ctime>
#include <vector>
#include <thread>
#include <sstream>
#include <list>
#include <Windows.h>

//NOTES!!:
// 1.scheduler works from initialization
//
// 2.scheduler needs time to pars the string and push tasks, so it will be unsafe to put tasks after up to 3 seconds after initialization(of scheduler)
//
// 3.scheduler works only for next 24 hours and if you initialize at 23:15:13 last task will be possible run at 23:15:12
//
// 4.use only catalogs/files without spaces (as sheduler should read diractory as word)
//
// 5.print_tasks work for next 24 hours so for example if you run print_tasks at 03:00:00
//  it will be incorrect to put time like from 01:00:00 to 04:00:00 (as 4 AM start sooner then 1 AM)
//  however it will be correct to put time from 23:00:00 to 01:00:00
//
// 6.scheduler doesnt throw anything 
//   however parsing throws std::string that describes the error(e.g. incorrect time, incorrect string)
// 
// 7.scheduler is not effected by changing system time during program


class task {
public:
	virtual void run() const = 0;
	virtual std::string name() const = 0;
};
class parsing;
class scheduler;

class print_tasks : public task {
public:
	scheduler *this_sched;
	time_t from;
	time_t to;
public:
	print_tasks(scheduler*, time_t&, time_t&);
	void run() const override;
	std::string name()const override;
};

class play_music : public task {
public:
	std::string music;
public:
	play_music(std::string&);
	void run() const override;
	std::string name() const override;
};

class copy_file : public task {
public:
	std::string from;
	std::string to;
public:
	copy_file(std::string&, std::string&);
	void run() const override;
	std::string name() const override;
};

class show_message : public task {
public:
	std::vector<std::string> message;
public:
	show_message(std::vector<std::string>&);
	void run() const override;
	std::string name() const override;
};

class open : public task {
public:
	std::string url;
public:
	open(std::string&);
	void run() const override;
	std::string name() const override;
};

class scheduler {
	std::map < time_t, std::vector<task*> > time_map;
	std::list<std::thread> thread_list;
	void join_all_threads();//need when scheduler ends
	const std::time_t starting_time; //time from which schedule will work
public:
	scheduler(time_t&);
	~scheduler();
	void push(const parsing&);
	void start_work();//after we pushed all the tasks
	std::time_t start_time();
	friend void print_tasks::run() const; //for map
};

class parsing {
	std::time_t time;
	std::string task;
	std::vector<std::string> param;
	const scheduler* this_sched;

	bool check_directory(std::string&) const;
	bool check_time(std::string&) const;
	bool check_url(std::string&) const;
public:
	parsing(scheduler*, std::string&);
	std::time_t get_time() const;
	std::string get_task() const;
	std::vector<std::string> get_param() const;
};


std::vector<std::string> split(std::string, char);///parcing:must scheduler:for time_in
std::time_t time_in(scheduler*, std::string); //parcing:must(can escape if impossible) scheduler:must