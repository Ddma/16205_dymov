package pvz;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import pvz.enumerations.Direction;
import pvz.gameObjects.*;
import pvz.rendering.Frame;

public class GameMap {
    public static final int SIZE_OF_MAP_X = 30;
    public static final int SIZE_OF_MAP_Y = 30;
    private static final int minNumOfBots = 8;
    private static GameObject[][] gameMap;
    private static List<GamePlayer> objects;
    private static List<GamePlayer> basket;
    private static List<Food> food;
    private static List<Poison> poison;

    private static boolean playing = true;
    private static int timesUsedUpdate = 0;
    private static int turnsFromStart = 0;


    private GameMap(){}

    static {
        gameMap = new GameObject[SIZE_OF_MAP_X][SIZE_OF_MAP_Y];
        objects = new ArrayList<>();
        basket = new ArrayList<>();
        food = new ArrayList<>();
        poison = new ArrayList<>();
    }

    public static boolean moveObject(GamePlayer obj,Coordinates toCoord){
        if(gameMap[toCoord.getX()][toCoord.getY()] != null)
            return false;
        gameMap[obj.getCoordinates().getX()][obj.getCoordinates().getY()] = null;
        gameMap[toCoord.getX()][toCoord.getY()] = obj;
        return true;
    }

    public static void deleteObject(GameObject obj) {
        if (obj instanceof GamePlayer)
            basket.add((GamePlayer)obj);//dont removing in case of last survivor (and possibility of concurrent exception)
        if(obj instanceof Food)
            food.remove(obj);
        if(obj instanceof Poison)
            poison.remove(obj);
        gameMap[obj.getCoordinates().getX()][obj.getCoordinates().getY()] = null;
    }

    public static boolean addObject(GameObject obj) {
        if(gameMap[obj.getCoordinates().getX()][obj.getCoordinates().getY()] != null)
            return false;
        gameMap[obj.getCoordinates().getX()][obj.getCoordinates().getY()] = obj;
        if (obj instanceof GamePlayer){
            objects.add((GamePlayer)obj);
        }
        if (obj instanceof Food){
            food.add((Food) obj);
        }
        if (obj instanceof Poison){
            poison.add((Poison)obj);
        }
        return true;
    }

    public static boolean isEmpty(Coordinates coordinates){
        if(gameMap[coordinates.getX()][coordinates.getY()] == null){
            return true;
        }
        return false;
    }

    public static Coordinates emptyAround(Coordinates coordinates){
        Coordinates newCoordinates;
        for (Direction dir : Direction.values()) {
            if( isEmpty(newCoordinates = coordinates.moveTo(Direction.TOP,dir)))
                return newCoordinates;
        }
        return null;
    }

    public static GameObject getVision(Coordinates coordinates){
        return gameMap[coordinates.getX()][coordinates.getY()];
    }

///////////////нет проверки out of range
    public static void updateLogic(){
        turnsFromStart++;
        if (objects.size() < minNumOfBots){
            timesUsedUpdate++;
            int survivors = objects.size();
            if(survivors==0) {
                while(!GameObject.create(new Coordinates((int)(Math.random()*100),(int)(Math.random()*100)),
                        GamePlayer.class,null,false)); //unlucky
                survivors = 1;
            }
            for (int num=0;num<survivors;num++){
                Coordinates baseCoordinate = objects.get(num).getCoordinates();
                int i=0;
                boolean mutation = false;
                if (i < 4){
                    if (GameObject.create(emptyAround(baseCoordinate), GamePlayer.class,objects.get(num), mutation))
                        i++;
                    else {
                        if(Math.random() > 0.5)
                            baseCoordinate = new Coordinates(baseCoordinate.getX() + 1, baseCoordinate.getY());
                        else if(Math.random() > 0.5)
                            baseCoordinate = new Coordinates(baseCoordinate.getX(),baseCoordinate.getY()+1);
                        else
                            baseCoordinate = new Coordinates(baseCoordinate.getX()+1,baseCoordinate.getY()+1);
                    }
                    if (i == 3)
                        mutation = true;
                }
            }
        }
        for(GamePlayer obj:objects){
            obj.doSomething();
        }
        for(GamePlayer obj:basket){
            objects.remove(obj);
        }
        basket.clear();

        if(food.size() + poison.size() < 40)
        {
            double rand;
            for (int y = 0; y < SIZE_OF_MAP_Y; y++)
                for (int x = 0; x < SIZE_OF_MAP_X; x++)
                    if (gameMap[x][y] == null)
                        if ((rand = Math.random()) < 0.05) //in average 800 empty cells and need to add around 40
                        {
                            if (rand > 0.020)
                                GameObject.create(new Coordinates(x,y),Poison.class);
                            else
                                GameObject.create(new Coordinates(x,y),Food.class);
                        }
        }


    }


    public static void stop(){
        playing = false;
    }
    public static void play(){
        playing = true;
    }
    public static boolean isPlaying(){
        return playing;
    }
    public static int getTimesUsedUpdate(){
        return timesUsedUpdate;
    }
    public static int getTurnsFromStart(){
        return turnsFromStart;
    }
    public static List<GamePlayer> getListOfBots(){
        return objects;
    }
    public static List<Food> getListOfFood(){
        return food;
    }
    public static List<Poison> getListOfPoison(){
        return poison;
    }
}
