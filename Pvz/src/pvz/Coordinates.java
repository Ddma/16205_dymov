package pvz;

import pvz.enumerations.Direction;

public class Coordinates {
    private final int x;
    private final int y;


    public Coordinates(int x,int y){
        if(x == -1)
            x=GameMap.SIZE_OF_MAP_X-1;
        if(y == -1)
            y=GameMap.SIZE_OF_MAP_Y-1;
        this.x = x % GameMap.SIZE_OF_MAP_X;
        this.y = y % GameMap.SIZE_OF_MAP_Y;
    }
    public int getX() {
        return x;
    }
    public int getY(){
        return y;
    }


    public Coordinates moveTo(Direction objectDirection, Direction movedDirection){

        Direction relativeToMapDirection = Direction.relativeToMapDirection(objectDirection, movedDirection);

        Coordinates newCoordinates = null;
        if(relativeToMapDirection == Direction.TOP)
            newCoordinates = new Coordinates(x,y+1);
        else if(relativeToMapDirection == Direction.TOP_RIGHT)
            newCoordinates = new Coordinates(x+1,y+1);
        else if(relativeToMapDirection == Direction.RIGHT)
            newCoordinates = new Coordinates(x+1,y);
        else if(relativeToMapDirection == Direction.DOWN_RIGHT)
            newCoordinates = new Coordinates(x+1,y-1);
        else if(relativeToMapDirection == Direction.DOWN)
            newCoordinates = new Coordinates(x,y-1);
        else if(relativeToMapDirection == Direction.DOWN_LEFT)
            newCoordinates = new Coordinates(x-1,y-1);
        else if(relativeToMapDirection == Direction.LEFT)
            newCoordinates = new Coordinates(x-1,y);
        else if(relativeToMapDirection == Direction.TOP_LEFT)
            newCoordinates = new Coordinates(x-1,y+1);
        return newCoordinates;
    }

}
