package pvz.commands;

import pvz.enumerations.Direction;

public class CommandTurn extends Command {

    public CommandTurn(Direction direction) {
        super(direction);
    }
}
