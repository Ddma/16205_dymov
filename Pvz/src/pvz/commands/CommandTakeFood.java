package pvz.commands;

import pvz.enumerations.Direction;

public class CommandTakeFood extends Command {

    public CommandTakeFood(Direction direction) {
        super(direction);
    }
}
