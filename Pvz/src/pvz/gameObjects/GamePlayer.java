package pvz.gameObjects;

import pvz.Coordinates;
import pvz.GameMap;
import pvz.commands.*;
import pvz.enumerations.Direction;
import pvz.rendering.CharacteristicPanel;

import java.awt.*;
import java.io.PrintWriter;

public class GamePlayer extends GameObject {
    private CommandCollection commands;
    private static final int maxNumCommandForOneTurn = 10;
    private final int maxEnergy = 80;
    private int turnsAlive = 0;

    private Direction direction = Direction.TOP;
    private int tempEnergy = maxEnergy;

    protected GamePlayer(Coordinates coordinates, GamePlayer parent, boolean mutation){
        super(coordinates);
        if (parent == null)
            commands = new CommandCollection();
        else
            commands = new CommandCollection(parent.getCommands(), mutation);
    }

    public CommandCollection getCommands() {
        return commands;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getTempEnergy(){
        return tempEnergy;
    }

    public Command getTempCommand(){
        return commands.currentCommand();
    }

    public int getTurnsAlive(){return turnsAlive;}

    public void doSomething(){
        turnsAlive++;
        for(int i=0;i<maxNumCommandForOneTurn;i++) {
            Command current = commands.currentCommand();
            GameObject obj = GameMap.getVision(getCoordinates().moveTo(direction, current.getDirection()));


            commands.movePointerTo(current.getPointerchange(obj));
            tempEnergy--;
            if(tempEnergy < 0){
                CharacteristicPanel.botDeadFromTiredness();
                die();
                return;
            }

            if (current instanceof CommandMove) {
                if (obj == null) {
                    changeCoordinates(getCoordinates().moveTo(direction, current.getDirection()));
//                    GameMap.moveObject(this,getCoordinates().moveTo(direction, current.getDirection()));
                    break;
                }
            } else if (current instanceof CommandTakeFood) {
                if(obj instanceof Food) {
                    tempEnergy+=10;
                    CharacteristicPanel.foodEaten();
                    GameMap.deleteObject(obj);
                    break;
                }
                else if (obj instanceof Poison){
                    CharacteristicPanel.poisonEaten();
                    GameMap.deleteObject(obj);
                    die();
                    break;
                }
            } else if (current instanceof CommandTurn) {
                direction = Direction.relativeToMapDirection(direction,current.getDirection());
            } else if (current instanceof CommandTurnPoisonToFood) {
                if(obj instanceof Poison){
                    CharacteristicPanel.foodTransformated();
                    GameMap.deleteObject(obj);
                    GameObject.create(obj.getCoordinates(),Food.class);
                    break;
                }
            } else if (current instanceof CommandWatch) {
                //nothing here (just moving pointer)
            }
        }
    }


    @Override
    public void paint(Graphics g){
        g.setColor(Color.BLACK);
        g.drawRect(50,5,20,20);
        g.setColor(Color.BLUE);
        g.fillRect(10,10,10,10);
    }
}
