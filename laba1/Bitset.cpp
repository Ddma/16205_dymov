#include "b.h"

//private function
int BitArray::num_of_1_in_1_byte(int i)
{
	int sum = 0;
	if (i == vect.size() - 1 && num % 8 != 0) { //last byte and not all bits
		for (int j = 0; j < num % 8; j++)
			if ((vect[i] >> (7 - j)) & 1)
				sum++;
		return sum;
	}
	if (vect[i])
		for (int j = 0; j < 8; j++)
			if ((vect[i] >> (7 - j)) & 1)
				sum++;
	return sum;
}




BitArray::BitArray() : num(0), num_of_1(0) {}

////BitArray has
////structure where higher bit has less value
////for example value=10 with 8 bits will have an array {0, 0, 0, 0, 1, 0, 1, 0}
////or value=1025 with 6 bits will have an arrat {0, 0, 0, 0, 0, 0, 0, 1}
BitArray::BitArray(int num_bits, unsigned long value) : num(num_bits), num_of_1(0)
{
	if (num_bits < 0)
		throw std::invalid_argument("initialization with negative size");
	if (num_bits > 0)
		vect.resize((num - 1) / 8 + 1);// for 8 bits - 1byte ; for 9 bits 2 bytes
	for (int i = num - 1; i >= 0 && value > 0; i--) { //moving from end to start  (when value = 0 all other bits will be 0, so no reason to continue)
		if (value & 1) {//moving bit to needed place in byte
			vect[i / 8] |= (1 << (7 - (i % 8)));//e.g.:(from left) [8] its first bit in 2nd byte ; [7] its last bit in byte
			num_of_1++;
		}
		value = value >> 1; //reducing the value
	}
}

BitArray::BitArray(const BitArray& b) : num(b.num), num_of_1(b.num_of_1), vect(b.vect) {}




BitArray::~BitArray() {}




BitArray& BitArray::set(int n, bool val) 
{
	bool prev_val = (*this)[n];
	unsigned char k = 1;
	k <<= (7 - n % 8); //k - its a place in byte in which we will put value
	if (val) {
		if (!prev_val) { //if was push 1 instead of 0
			num_of_1++;
			vect[n / 8] |= k;
		}
	}
	else {
		if (prev_val) { //if was push 0 instead of 1
			num_of_1--;
			vect[n / 8] &= ~k;
		}
	}
	return *this;
}

BitArray& BitArray::set()
{
	for (int i = 0; i < (int)vect.size(); i++)
		vect[i] = 255; //255 when all elements are true in 8 bits
	num_of_1 = num;
	return *this;
}

BitArray& BitArray::reset(int n)
{
	if ((*this)[n]) {//check id
		vect[n / 8] &= ~(1 << (7 - (n % 8)));
		num_of_1--;
	}
	return *this;//if its already false no need in change
}

BitArray& BitArray::reset()
{
	for (int i = 0; i < (int)vect.size(); i++)
		vect[i] = 0;
	num_of_1 = 0;
	return *this;
}

void BitArray::resize(int num_bits, bool value)
{
	if (num_bits < 0)
		throw std::invalid_argument("resize in negative size");
	if (num_bits < num) {
		for (int i = num - 1; i >= num_bits; i--) {
			if ((*this)[i])//check every bit we will delete
				num_of_1--;
		}
		num = num_bits;
		vect.resize((num_bits - 1) / 8 + 1);
		return;
	}
	else if (num_bits > num) {
		if (num != 0) {
			//filling last byte as vector cant fill it
			for (int i = num % 8; i < 8; i++) {
				if (value)
					vect[vect.size() - 1] |= (1 << (7 - i));//fill rest of the byte with 1
				else
					vect[vect.size() - 1] &= ~(1 << (7 - i));//fill rest of the byte with 0
			}
		}
		unsigned char fill_this = 0;
		if (value)
			fill_this = ~fill_this;
		vect.resize((num_bits - 1) / 8 + 1, fill_this);
		if (value)
			num_of_1 += num_bits - num;
		num = num_bits;
		return;
	}
}

void BitArray::clear()
{
	vect.resize(0);
	num = num_of_1 = 0;
}

void BitArray::push_back(bool bit)
{
	if (num % 8 == 0) { //if so new bit will be in newbyte
		unsigned char first_bit;
		if (bit)
			first_bit = 128;// first bit true in new byte
		else
			first_bit = 0;// first bit false in new byte
		vect.push_back(first_bit);
	}
	else {
		unsigned char k = 1 << (7 - (num % 8));//needed place
		if (bit)
			vect[vect.size() - 1] |= k;
		else
			vect[vect.size() - 1] &= ~k;
	}
	if (bit)
		num_of_1++;
	num++;
}

void BitArray::swap(BitArray& b)
{
	std::swap(num, b.num);
	std::swap(num_of_1, b.num_of_1);
	vect.swap(b.vect);
}




bool BitArray::any() const
{
	if (num_of_1)
		return true;
	else
		return false;
}

bool BitArray::none() const
{
	if (num_of_1 == 0)
		return true;
	else
		return false;
}

int BitArray::count() const
{
	return num_of_1;
}

int BitArray::size() const
{
	return num;
}

bool BitArray::empty() const
{
	if (!num)
		return true;
	else
		return false;
}

bool BitArray::operator[](int i) const 
{
	if (i < 0)
		throw std::out_of_range("out of range");
	int byte = i / 8;//value in this byte
	int bit = i % 8;//value in this bit
	if (num <= 8 * byte + bit)
		throw std::out_of_range("out of range");
	if ((vect[byte] >> (7 - bit)) & 1)// move needed bit to last place in byte
		return true;
	else
		return false;
}

std::string BitArray::to_string() const
{
	std::string str(num, '0');
	for (int i = 0; i < (int)vect.size(); i++)
		if (vect[i])//check for having at least one true element
			for (int j = 0; j < 8 && 8 * i + j < num; j++)
				if ((vect[i] >> (7 - j)) & 1)
					str[8 * i + j] = '1';
	return str;

}




BitArray& BitArray::operator=(const BitArray& b)
{
	num = b.num;
	num_of_1 = b.num_of_1;
	vect = b.vect;
	return (*this);
}

BitArray BitArray::operator~() const
{
	BitArray inv(num);
	for (int i = 0; i < (int)vect.size(); i++) {
		inv.vect[i] = ~vect[i];
	}
	inv.num_of_1 = num - num_of_1;
	return inv;
}

BitArray& BitArray::operator&=(const BitArray& b)
{
	if (num != b.num)
		throw std::invalid_argument("different size");
	num_of_1 = 0;
	for (int i = 0; i < (int)vect.size(); i++) {
		vect[i] &= b.vect[i];
		num_of_1 += num_of_1_in_1_byte(i);
	}
	return *this;
}

BitArray& BitArray::operator|=(const BitArray& b)
{
	if (num != b.num)
		throw std::invalid_argument("different size");
	num_of_1 = 0;
	for (int i = 0; i < (int)vect.size(); i++) {
		vect[i] |= b.vect[i];
		num_of_1 += num_of_1_in_1_byte(i);
	}
	return *this;
}

BitArray& BitArray::operator^=(const BitArray& b)
{
	if (num != b.num)
		throw std::invalid_argument("different size");
	num_of_1 = 0;
	for (int i = 0; i < (int)vect.size(); i++) {
		vect[i] ^= b.vect[i];
		num_of_1 += num_of_1_in_1_byte(i);
	}
	return *this;
}

BitArray& BitArray::operator<<=(int n)
{
	if (n < 0)
		throw std::invalid_argument("negative size");
	if (n == 0)
		return *this;
	int byte = n / 8;//count of moving bytes
	int bit = n % 8;//count of moving bits
	if (byte >= (int)vect.size()) {//make it zero
		num_of_1 = num = 0;
		for (int i = 0; i < (int)vect.size(); i++)
			vect[i] = 0;
		return *this;
	}
	for (int i = 0; i < byte; i++) {//counting missing 1 in bytes
		num_of_1 -= num_of_1_in_1_byte(i);
	}
	if (num % 8 != 0) {//just make sure that in last byte not our bits are zeros
		vect[vect.size() - 1] >>= (8 - num % 8);
		vect[vect.size() - 1] <<= (8 - num % 8);
	}
	//moving
	for (int j = 0; j < bit; j++)//counting missing 1 in first byte  
		if ((vect[byte] >> (7 - j)) & 1)
			num_of_1--;
	int i = 0;
	for (; i < (int)vect.size() - (byte + 1); i++) {
		vect[i] = vect[i + byte] << bit;
		vect[i] |= vect[i + byte + 1] >> (8 - bit);
	}
	vect[i] = vect[i + byte] << bit;//[i+byte] previously last meaning byte, so we shouldnt add anything
	for (int i = vect.size() - byte; i < (int)vect.size(); i++)//rest are zeros
		vect[i] = 0;
	return *this;
}

BitArray& BitArray::operator>>=(int n)
{
	if (n < 0)
		throw std::invalid_argument("negative size");
	if (n == 0)
		return *this;
	int byte = n / 8;//count of moving bytes
	int bit = n % 8;//count of moving bits
	if (byte >= (int)vect.size()) {//make it zero
		num_of_1 = num = 0;
		for (int i = 0; i < (int)vect.size(); i++)
			vect[i] = 0;
		return *this;
	}
	for (int i = 1; i <= byte; i++) {//counting missing 1 in bytes
		num_of_1 -= num_of_1_in_1_byte(vect.size() - i);
	}
	//moving
	for (int j = 0; j < bit + ((8 - num % 8 == 8) ? 0 : (8 - num % 8)); j++)//counting missing 1 in last (in future) byte  
		if ((vect[vect.size() - 1 - byte] >> j) & 1)// with ?: we subtract bits that will be out of range in future byte (if n%8==0 no need in this, so it equals 0)
			num_of_1--;
	int i = vect.size() - 1;
	for (; i >= byte + 1; i--) {
		vect[i] = vect[i - byte] >> bit;
		vect[i] |= vect[i - byte - 1] << (8 - bit);
	}
	vect[i] = vect[i - byte] >> bit;//[i-byte] previously first meaning byte, so we shouldnt add anything more from left
	for (int i = byte - 1; i >= 0; i--) //rest are zeros
		vect[i] = 0;
	return *this;
}

BitArray BitArray::operator<<(int n) const
{
	if (n < 0)
		throw std::invalid_argument("negative size");
	BitArray newarr(*this);
	newarr <<= n;
	return newarr;
}

BitArray BitArray::operator >> (int n) const
{
	if (n < 0)
		throw std::invalid_argument("negative size");
	BitArray newarr(*this);
	newarr >>= n;
	return newarr;
}




bool operator!=(const BitArray &a, const BitArray &b)
{
	if (a.count() != b.count())
		return true;
	if (a.size() != b.size())
		return true;
	for (int i = 0; i < a.size(); ++i) {
		if (a[i] != b[i])
			return true;
	}
	return false;
}

bool operator==(const BitArray &a, const BitArray &b)
{
	if (a != b)
		return false;
	return true;
}

BitArray operator&(const BitArray& b1, const BitArray& b2)
{
	BitArray newarr(b1);
	newarr &= b2;
	return newarr;
}

BitArray operator|(const BitArray& b1, const BitArray& b2)
{
	BitArray newarr(b1);
	newarr |= b2;
	return newarr;
}

BitArray operator^(const BitArray& b1, const BitArray& b2)
{
	BitArray newarr(b1);
	newarr ^= b2;
	return newarr;
}
