#include "gtest\gtest.h"
#include "b.h"
#include <exception>

//before testing methods 
//1.to_string
//2.count
//3.size
//should work right

TEST(Constructor_tests, throw_test) {
	EXPECT_THROW(BitArray arr1(-5), std::invalid_argument);
}

TEST(Constructor_tests, proper_initialization) {
	BitArray arr1(10);
	EXPECT_EQ(arr1.to_string(), "0000000000");
	EXPECT_EQ(arr1.count(), 0);
	EXPECT_EQ(arr1.size(), 10);

	BitArray arr2(10, 1024);
	EXPECT_EQ(arr2, arr1);

	BitArray arr3(13, 5433);// 1010100111001_2 (13 bits)
	EXPECT_EQ(arr3.to_string(), "1010100111001");
	EXPECT_EQ(arr3.count(), 7);
	EXPECT_EQ(arr3.size(), 13);

	BitArray arr4(arr3);
	EXPECT_EQ(arr4, arr3);

	BitArray arr5(23, 31895359);//31895359_10 == 1111001101010111100111111_2 (25 bits)
	EXPECT_EQ(arr5.to_string(), "11001101010111100111111");
	EXPECT_EQ(arr5.count(), 16);
	EXPECT_EQ(arr5.size(), 23);



	BitArray arr6(70, ULONG_MAX); //Ulong 32 bits
	std::string  strUL = "000000";
	for (int i = 0; i < 4; ++i)
		strUL += "00000000";
	for (int i = 0; i < 4; ++i)
		strUL += "11111111";
	EXPECT_EQ(arr6.to_string(),strUL);
	EXPECT_EQ(arr6.count(), 32);
	EXPECT_EQ(arr6.size(), 70);

	BitArray arr7(0);
	EXPECT_EQ(arr7.to_string(), "");
	EXPECT_EQ(arr7.count(), 0);
	EXPECT_EQ(arr7.size(), 0);
}

TEST(Set_reset, throw_test) {
	BitArray arr1(10);
	EXPECT_THROW(arr1.set(-1, true), std::out_of_range);
	EXPECT_THROW(arr1.set(10, false), std::out_of_range);
	EXPECT_THROW(arr1.reset(-5), std::out_of_range);
	EXPECT_THROW(arr1.reset(13), std::out_of_range);
}

TEST(Set_reset, logic_test) {
	//if bit is false
	BitArray arr1(10);
	arr1.set(3, true);
	EXPECT_EQ(arr1[3], true);
	EXPECT_EQ(arr1.count(), 1);
	arr1.reset(3);
	EXPECT_EQ(arr1[3], false);
	EXPECT_EQ(arr1.count(), 0);

	arr1.set(9, false);
	EXPECT_EQ(arr1[9], false);
	EXPECT_EQ(arr1.count(), 0);
	arr1.reset(9);
	EXPECT_EQ(arr1[9], false);
	EXPECT_EQ(arr1.count(), 0);

	// if bit is true
	arr1.set(7, true);//already checked
	arr1.set(7, true);
	EXPECT_EQ(arr1[7], true);
	EXPECT_EQ(arr1.count(), 1);
	arr1.set(7, false);
	EXPECT_EQ(arr1[7], false);
	EXPECT_EQ(arr1.count(), 0);

	BitArray arr2(13, 5433);//   1010100111001_2 (13 bits)
	arr2.set();
	EXPECT_EQ(arr2.to_string(), "1111111111111");
	EXPECT_EQ(arr2.count(), 13);
	EXPECT_EQ(arr2.size(), 13);
	arr2.reset();
	EXPECT_EQ(arr2.to_string(), "0000000000000");
	EXPECT_EQ(arr2.count(), 0);
	EXPECT_EQ(arr2.size(), 13);

	BitArray arr3;
	arr3.set();
	EXPECT_EQ(arr3.to_string(), "");
	EXPECT_EQ(arr3.count(), 0);
	EXPECT_EQ(arr3.size(), 0);
	arr3.reset();
	EXPECT_EQ(arr3.to_string(), "");
	EXPECT_EQ(arr3.count(), 0);
	EXPECT_EQ(arr3.size(), 0);
}

TEST(Another_setters, throw_test) {
	BitArray arr1(10);
	EXPECT_THROW(arr1.resize(-2, false), std::invalid_argument);
}

TEST(Another_setters, logic_test) {
	BitArray arr1(10);
	arr1.resize(1);
	EXPECT_EQ(arr1.to_string(), "0");
	EXPECT_EQ(arr1.count(), 0);
	EXPECT_EQ(arr1.size(), 1);
	arr1.resize(10, false);
	EXPECT_EQ(arr1.to_string(), "0000000000");
	EXPECT_EQ(arr1.count(), 0);
	EXPECT_EQ(arr1.size(), 10);

	BitArray arr2(13, 5433);//   1010100111001_2 (13 bits)
	arr2.resize(10);
	arr2.resize(13,true);
	EXPECT_EQ(arr2.to_string(), "1010100111111");
	EXPECT_EQ(arr2.count(), 9);
	EXPECT_EQ(arr2.size(), 13);
	arr2.clear();
	EXPECT_EQ(arr2.to_string(), "");
	EXPECT_EQ(arr2.count(), 0);
	EXPECT_EQ(arr2.size(), 0);

	BitArray arr3(10);
	arr3.push_back(true);
	EXPECT_EQ(arr3.to_string(), "00000000001");
	EXPECT_EQ(arr3.count(), 1);
	EXPECT_EQ(arr3.size(), 11);
	arr3.push_back(false);
	EXPECT_EQ(arr3.to_string(), "000000000010");
	EXPECT_EQ(arr3.count(), 1);
	EXPECT_EQ(arr3.size(), 12);

	arr1.swap(arr3);
	EXPECT_EQ(arr3.to_string(), "0000000000");
	EXPECT_EQ(arr3.count(), 0);
	EXPECT_EQ(arr3.size(), 10);
	EXPECT_EQ(arr1.to_string(), "000000000010");
	EXPECT_EQ(arr1.count(), 1);
	EXPECT_EQ(arr1.size(), 12);
}

TEST(Operations, throw_test) {//different sizes & negative parametrs
	BitArray arr1(10);
	BitArray arr2(15);
	EXPECT_THROW(arr1 &= arr2, std::invalid_argument);
	EXPECT_THROW(arr1 |= arr2, std::invalid_argument);
	EXPECT_THROW(arr1 ^= arr2, std::invalid_argument);
	EXPECT_THROW(arr1 >>= -4, std::invalid_argument);
	EXPECT_THROW(arr1 <<= -1, std::invalid_argument);
	EXPECT_THROW(arr1 >> -4, std::invalid_argument);
	EXPECT_THROW(arr1 << -1, std::invalid_argument);
	EXPECT_THROW(arr1 & arr2, std::invalid_argument);
	EXPECT_THROW(arr1 | arr2, std::invalid_argument);
	EXPECT_THROW(arr1 ^ arr2, std::invalid_argument);
}

TEST(Operations, logic_test) {
	BitArray arr1(13, 5433);//   1010100111001_2 (13 bits)
	BitArray arr2 = ~arr1;
	EXPECT_EQ(arr2.to_string(), "0101011000110");
	EXPECT_EQ(arr2.count(), 6);
	BitArray arr3;
	EXPECT_EQ((~arr3).to_string(), "");
	EXPECT_EQ(arr3.count(), 0);

	BitArray arr4(13, 5433);// 1010100111001_2 (13 bits)
	BitArray arr5(13, 8191);// 1111111111111_2
	BitArray arr6;

	//&,|^ use operations ^=,&=,|= so no need in checking
	arr6 = arr4 & arr5;
	EXPECT_EQ(arr6.to_string(), "1010100111001");
	EXPECT_EQ(arr6.count(), 7);
	arr6 = arr4 | arr5;
	EXPECT_EQ(arr6.to_string(), "1111111111111");
	EXPECT_EQ(arr6.count(), 13);
	arr6 = arr4 ^ arr5;
	EXPECT_EQ(arr6.to_string(), "0101011000110");
	EXPECT_EQ(arr6.count(), 6);

	BitArray arr7(13, 5433);// 1010100111001_2 (13 bits)
	BitArray arr8;

	//>>,<< use operations >>=,<<= so no need in checking
	arr8 = arr7 << 0;
	EXPECT_EQ(arr8.to_string(), "1010100111001");
	EXPECT_EQ(arr8.count(), 7);
	arr8 = arr7 >> 0;
	EXPECT_EQ(arr8.to_string(), "1010100111001");
	EXPECT_EQ(arr8.count(), 7);

	arr8 = arr7 << 8;
	EXPECT_EQ(arr8.to_string(), "1100100000000");
	EXPECT_EQ(arr8.count(), 3);
	arr8 = arr7 >> 8;
	EXPECT_EQ(arr8.to_string(), "0000000010101");
	EXPECT_EQ(arr8.count(), 3);

	arr8 = arr7 << 10;
	EXPECT_EQ(arr8.to_string(), "0010000000000");
	EXPECT_EQ(arr8.count(), 1);
	arr8 = arr7 >> 10;
	EXPECT_EQ(arr8.to_string(), "0000000000101");
	EXPECT_EQ(arr8.count(), 2);

	arr8 = arr7 << 15;
	EXPECT_EQ(arr8.to_string(), "0000000000000");
	EXPECT_EQ(arr8.count(), 0);
	arr8 = arr7 >> 15;
	EXPECT_EQ(arr8.to_string(), "0000000000000");
	EXPECT_EQ(arr8.count(), 0);
}


//just to make sure (as most hardest function)
TEST(Operations, my_test){
	BitArray arr1(23, 31895359);//31895359_10 == 1111001101010111100111111_2 (25 bits)
	BitArray arr2;
	arr2 = arr1 >> 4;
	EXPECT_EQ(arr2.to_string(), "00001100110101011110011");
	EXPECT_EQ(arr2.count(), 12);
	arr2 = arr1 >> 10;
	EXPECT_EQ(arr2.to_string(), "00000000001100110101011");
	EXPECT_EQ(arr2.count(), 8);
	arr2 = arr1 >> 15;
	EXPECT_EQ(arr2.to_string(), "00000000000000011001101");
	EXPECT_EQ(arr2.count(), 5);
	arr2 = arr1 >> 16;
	EXPECT_EQ(arr2.to_string(), "00000000000000001100110");
	EXPECT_EQ(arr2.count(), 4);
	arr2 = arr1 >> 17;
	EXPECT_EQ(arr2.to_string(), "00000000000000000110011");
	EXPECT_EQ(arr2.count(), 4);
	arr2 = arr1 >> 20;
	EXPECT_EQ(arr2.to_string(), "00000000000000000000110");
	EXPECT_EQ(arr2.count(), 2);

	arr2 = arr1 << 4;
	EXPECT_EQ(arr2.to_string(), "11010101111001111110000");
	EXPECT_EQ(arr2.count(), 14);
	arr2 = arr1 << 10;
	EXPECT_EQ(arr2.to_string(), "01111001111110000000000");
	EXPECT_EQ(arr2.count(), 10);
	arr2 = arr1 << 15;
	EXPECT_EQ(arr2.to_string(), "00111111000000000000000");
	EXPECT_EQ(arr2.count(), 6);
	arr2 = arr1 << 16;
	EXPECT_EQ(arr2.to_string(), "01111110000000000000000");
	EXPECT_EQ(arr2.count(), 6);
	arr2 = arr1 << 17;
	EXPECT_EQ(arr2.to_string(), "11111100000000000000000");
	EXPECT_EQ(arr2.count(), 6);
	arr2 = arr1 << 20;
	EXPECT_EQ(arr2.to_string(), "11100000000000000000000");
	EXPECT_EQ(arr2.count(), 3);
}

int main(int argc, char* argv[]) {
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}