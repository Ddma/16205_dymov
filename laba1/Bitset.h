#include <iostream>
#include <vector>
#include <exception>

class BitArray
{
	int num;
	int num_of_1;
	std::vector<unsigned char>vect;
	int num_of_1_in_1_byte(int i);
public:
	BitArray();
	explicit BitArray(int num_bits, unsigned long value = 0);//create & initialize with value
	BitArray(const BitArray& b);

	~BitArray();

	//setters
	BitArray& set(int n, bool val = true);//set in n-bit value
	BitArray& set(); //fill an array with true
	BitArray& reset(int n);////set in n-bit false
	BitArray& reset();//fill an array with false
	void resize(int num_bits, bool value = false); //change the size, if it expands new elements will be initialized by value
	void clear();
	void push_back(bool bit);//add new bit to the end
	void swap(BitArray& b);

	//getters
	bool any() const;//true if an array have at least one true bit
	bool none() const;//true if all bits are false
	int count() const;//number of true bits
	int size() const;
	bool empty() const;
	bool operator[](int i) const;//return value of the n-bit
	std::string to_string() const;


	//operations
	BitArray& operator= (const BitArray& b);
	BitArray operator~() const;//return inversion of the array
	//Bit operations works only with Arrays with equal size 
	BitArray& operator&=(const BitArray& b);
	BitArray& operator|=(const BitArray& b);
	BitArray& operator^=(const BitArray& b);
	//shifts
	BitArray& operator<<=(int n);
	BitArray& operator>>=(int n);
	BitArray operator<<(int n) const;
	BitArray operator>> (int n) const;
};

bool operator!=(const BitArray &a, const BitArray &b);
bool operator==(const BitArray &a, const BitArray &b);
BitArray operator&(const BitArray& b1, const BitArray& b2);
BitArray operator|(const BitArray& b1, const BitArray& b2);
BitArray operator^(const BitArray& b1, const BitArray& b2);