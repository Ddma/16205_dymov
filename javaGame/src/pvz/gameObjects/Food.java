package pvz.gameObjects;

import pvz.Coordinates;

import java.awt.*;

public class Food extends GameObject {

    protected Food(Coordinates coordinates){
        super(coordinates);
    }

    @Override
    public void paint(Graphics g){
        g.setColor(Color.RED);
        g.fillRect(10,10,10,10);
        g.setColor(Color.BLACK);
        g.drawRect(10,10,10,10);
    }
}
