package pvz.gameObjects;

import pvz.Coordinates;

import java.awt.*;

public class Poison extends GameObject {

    protected Poison(Coordinates coordinates){
        super(coordinates);
    }


    @Override
    public void paint(Graphics g){
        g.setColor(Color.GREEN);
        g.fillRect(10,10,10,10);
        g.setColor(Color.BLACK);
        g.drawRect(10,10,10,10);
    }
}
