package pvz.gameObjects;

import pvz.Coordinates;
import pvz.GameMap;

import java.awt.*;


public abstract class GameObject{

    abstract public void paint(Graphics g);


    private Coordinates coordinates;


    protected GameObject(Coordinates coordinates){
        this.coordinates = coordinates;
    }

    protected void die(){
        GameMap.deleteObject(this);
    }

    protected boolean changeCoordinates(Coordinates newCoordinates){
        if(GameMap.moveObject((GamePlayer)this,newCoordinates)) {
            coordinates = newCoordinates;
            return true;
        }
        return false;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }


    //returns true if added
    //also adds to Game Map
    public static boolean create(Coordinates coordinates,Class<? extends GameObject> newObj,Object... extraParam){
        if(coordinates == null)
            return false;
        if(GameMap.isEmpty(coordinates)){
            GameObject obj = null;
            if (newObj.isAssignableFrom(Wall.class)) {
                obj = new Wall(coordinates);
            }
            else if(newObj.isAssignableFrom(Food.class)) {
                obj = new Food(coordinates);
            }
            else if(newObj.isAssignableFrom(Poison.class)) {
                obj = new Poison(coordinates);
            }
            else if(newObj.isAssignableFrom(GamePlayer.class)){
                if(extraParam.length == 2 && extraParam[0] instanceof GamePlayer && extraParam[1] instanceof Boolean)
                    obj = new GamePlayer(coordinates, (GamePlayer)extraParam[0],(Boolean)extraParam[1]);
                else
                    obj = new GamePlayer(coordinates, null,false);
            }
            if (obj == null)//How it can be possible????
                throw new NullPointerException("Class is not acceptable at creation");
            GameMap.addObject(obj);
            return true;
        }
        else {
            return false;
        }
    }

}
