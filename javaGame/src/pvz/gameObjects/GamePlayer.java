package pvz.gameObjects;

import pvz.Coordinates;
import pvz.GameMap;
import pvz.commands.*;
import pvz.enumerations.Direction;
import pvz.rendering.CharacteristicPanel;

import java.awt.*;
import java.io.PrintWriter;

public class GamePlayer extends GameObject {
    private CommandCollection commands;
    private final int maxEnergy = 40;
    private int turnsAlive = 0;

    private Direction direction = Direction.TOP;
    private int tempEnergy = maxEnergy;

    protected GamePlayer(Coordinates coordinates, GamePlayer parent, boolean mutation) {
        super(coordinates);
        if (parent == null)
            commands = new CommandCollection();
        else
            commands = new CommandCollection(parent.getCommands(), mutation);
    }

    public CommandCollection getCommands() {
        return commands;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getTempEnergy() {
        return tempEnergy;
    }

    public Command getTempCommand() {
        return commands.currentCommand();
    }

    public int getTurnsAlive() {
        return turnsAlive;
    }

    //using before new cycle
    public void restoreEnergy() {
        tempEnergy = maxEnergy;
    }

    //job for every cycle
    public void doSomething() {
        turnsAlive++;
        Command current = commands.currentCommand();
        GameObject obj = GameMap.getVision(getCoordinates().moveTo(current.getDirection()));


        commands.movePointerTo(current.getPointerchange(obj));
        tempEnergy--;
        if (tempEnergy < 0) {
            CharacteristicPanel.botDeadFromTiredness();
            die();
            return;
        }
        if (current instanceof CommandMove) {
            if (obj == null) {
                changeCoordinates(getCoordinates().moveTo(current.getDirection()));
            }
        } else if (current instanceof CommandTakeFood) {
            if (obj instanceof Food) {
                tempEnergy += 10;
                CharacteristicPanel.foodEaten();
                GameMap.deleteObject(obj);
            } else if (obj instanceof Poison) {
                CharacteristicPanel.poisonEaten();
                GameMap.deleteObject(obj);
                die();
            }
        } else if (current instanceof CommandTurnPoisonToFood) {
            if (obj instanceof Poison) {
                CharacteristicPanel.foodTransformated();
                GameMap.deleteObject(obj);
                GameObject.create(obj.getCoordinates(), Food.class);
            }
        }
        else
            System.out.println("ALERT");
    }



    @Override
    public void paint(Graphics g){
        g.setColor(Color.BLACK);
        g.drawRect(50,5,20,20);
        g.setColor(Color.BLUE);
        g.fillRect(10,10,10,10);
    }
}
