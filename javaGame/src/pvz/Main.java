package pvz;

import pvz.gameObjects.*;
import pvz.commands.*;
import pvz.enumerations.*;
import pvz.rendering.Frame;
import pvz.rendering.GraphComponent;
import pvz.rendering.GraphFrame;


import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.PrintWriter;


public class Main {
    public static void main(String[] args) {
        System.out.println("Start");

        GameObject.create(new Coordinates(2,2), Food.class);
        GameObject.create(new Coordinates(3,4), Poison.class);
        GameObject.create(new Coordinates(10,10),GamePlayer.class,null,false);


        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Frame frame = new Frame();
            }
        });
        try {
            Thread.sleep(3000);
        }catch (Exception e){}

        while (true) {
            if(GameMap.isPlaying()) {
                GameMap.updateLogic();
                Frame.updateGraphics();
                GraphFrame.updateGraphic();
            }
            try {
                Thread.sleep(100);
            }catch (Exception e){}
        }
        /*
        try {
            File dir = new File(System.getProperty("user.dir"));
            File logDir = new File(dir, "loggingDirectory");
            logDir.mkdir();
            PrintWriter logOut = new PrintWriter(new File(logDir, "log.txt"));
            PrintWriter logBot = new PrintWriter(new File(logDir, "logBot.txt"));

            GameObject.create(new Coordinates(2,2), Food.class);
            GameObject.create(new Coordinates(3,4), Poison.class);
            GameObject.create(new Coordinates(2,3), GamePlayer.class);
            GameObject.create(new Coordinates(4,6), GamePlayer.class);
            GameMap.updateGraphics(logOut);





            GameMap.updateLogic(logBot);
            GameMap.updateGraphics(logOut);
            logOut.close();
            logBot.close();
        }
        catch (Exception ex){
            System.out.println("Exception!!!!");
        }
        */
    }
}
