package pvz.enumerations;

public enum Direction {
    TOP(0), TOP_RIGHT(1), RIGHT(2), DOWN_RIGHT(3),
    DOWN(4), DOWN_LEFT(5), LEFT(6), TOP_LEFT(7);

    private int directionNumber;

    Direction(int num){
        directionNumber = num;
    }
    public int getDirectionNumber(){
        return directionNumber;
    }

    public static Direction getDirection(int number) {
        if (number == 0)
            return TOP;
        else if (number == 1)
            return TOP_RIGHT;
        else if (number == 2)
            return RIGHT;
        else if (number == 3)
            return DOWN_RIGHT;
        else if (number == 4)
            return DOWN;
        else if (number == 5)
            return DOWN_LEFT;
        else if (number == 6)
            return LEFT;
        else
            return TOP_LEFT;
    }

    public static Direction getRandomDirection(){
        double random = Math.random();
        if(random < (double)1/8)
            return TOP;
        else if(random < (double)2/8)
            return TOP_RIGHT;
        else if(random < (double)3/8)
            return RIGHT;
        else if(random < (double)4/8)
            return DOWN_RIGHT;
        else if(random < (double)5/8)
            return DOWN;
        else if(random < (double)6/8)
            return DOWN_LEFT;
        else if(random < (double)7/8)
            return LEFT;
        else
            return TOP_LEFT;
    }

    @Override
    public String toString(){
        if(this == TOP)
            return "↑";
        else if(this == TOP_RIGHT){
            return "↗";
        }
        else if(this == RIGHT){
            return "→";
        }
        else if(this == DOWN_RIGHT){
            return "↘";
        }
        else if(this == DOWN){
            return "↓";
        }
        else if(this == DOWN_LEFT){
            return "↙";
        }
        else if(this == LEFT){
            return "←";
        }
        else if(this == TOP_LEFT){
            return "↖";
        }
        else
            return "";
    }

}
