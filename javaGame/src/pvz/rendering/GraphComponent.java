package pvz.rendering;

import javax.swing.*;
import java.awt.*;

public class GraphComponent extends JComponent {

    private static int[] valuesOfCycles = new int[GraphFrame.WIDTH - 10];
    private static int numberOfCycles = 0;
    private static int maxValue = 0;


    private static final int startHeight = GraphFrame.HEIGHT - 40;
    private static final int startWidth = 15;

    @Override
    public synchronized void paint(Graphics g) {
        super.paint(g);
        g.drawLine(startWidth, startHeight, GraphFrame.WIDTH, startHeight);
        g.drawString("cycles", GraphFrame.WIDTH - 40, startHeight - 5);
        g.drawString(Integer.toString(numberOfCycles),GraphFrame.WIDTH - 30,startHeight+10);

        g.drawLine(startWidth, startHeight, startWidth, 0);
        g.drawString("turns", startWidth + 5, 10);
        g.drawString(Integer.toString(maxValue),startWidth - 15,10);


        g.drawLine(startWidth, startHeight, startWidth + 1, startHeight - fromValueToHeight(valuesOfCycles[0]));
        for (int i = 1; i < valuesOfCycles.length; i++) {
            if(valuesOfCycles[i] == 0)
                return;
            g.drawLine(startWidth + i, startHeight - fromValueToHeight(valuesOfCycles[i - 1]), startWidth + i + 1,
                    startHeight - fromValueToHeight(valuesOfCycles[i]));
        }
    }


    public synchronized static void getNewValue(int value) {
        if (value > maxValue)
            maxValue = value;
        numberOfCycles++;
        if (numberOfCycles <= valuesOfCycles.length) {
            valuesOfCycles[numberOfCycles - 1] = value;
            return;
        }
/*
        for(int i=0;i<valuesOfCycles.length - 1;i++){
            valuesOfCycles[i] = valuesOfCycles[i+1];
        }
        valuesOfCycles[valuesOfCycles.length-1] = value;
        */
        int shiftFromeHere;
        if (numberOfCycles % valuesOfCycles.length == 0) {
            valuesOfCycles[valuesOfCycles.length - 1] =
                    (valuesOfCycles[valuesOfCycles.length - 1] + value) / 2;
            return;
        }
        else {
            valuesOfCycles[numberOfCycles % valuesOfCycles.length] =
                    (valuesOfCycles[numberOfCycles % valuesOfCycles.length] + valuesOfCycles[numberOfCycles % valuesOfCycles.length - 1]) / 2;
            shiftFromeHere = numberOfCycles % valuesOfCycles.length;
        }

        for(int i=shiftFromeHere;i<valuesOfCycles.length-1;i++){
            valuesOfCycles[i] = valuesOfCycles[i+1];
        }
        valuesOfCycles[valuesOfCycles.length-1] = value;
    }


    public int fromValueToHeight(int value) {
        if(startHeight>maxValue)
            return (startHeight / maxValue) * value;
        else
            return value/((maxValue/startHeight) + 1);
    }
}
