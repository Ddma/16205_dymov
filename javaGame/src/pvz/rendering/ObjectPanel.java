package pvz.rendering;

import pvz.gameObjects.GameObject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ObjectPanel extends JPanel{

    private GameObject obj;

    public ObjectPanel(){
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
    }

    public void changeState(GameObject obj){
        if(this.obj != obj) {
            this.obj = obj;
            repaint();
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(30,30);
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        if(obj!=null) {
            obj.paint(g);
        }
    }
}
