package pvz.rendering;

import pvz.Coordinates;
import pvz.GameMap;

import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {

 //   GridLayout gridLayout = new GridLayout(30,30);
    private static ObjectPanel[][] objPanel = new ObjectPanel[30][30];
    private static CharacteristicPanel chPanel = new CharacteristicPanel();

    public Frame() {
        super("Game");
        setSize(1500, 1500);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel all = new JPanel();
        all.setLayout(new BorderLayout());

        JPanel map = new JPanel();

        map.setPreferredSize(new Dimension(900,900));
        map.setLayout(new GridLayout(30,30));
        for (int i = 0; i < 30; i++) {
            for (int j = 0; j < 30; j++) {
                objPanel[i][j] = new ObjectPanel();
                map.add(objPanel[i][j]);
            }
        }


        all.add(map,BorderLayout.CENTER);
        all.add(chPanel,BorderLayout.EAST);
        all.add(new ControlPanel(),BorderLayout.SOUTH);
        add(all);
        pack();
        setVisible(true);
    }


    public static void updateGraphics() {
        for (int y = 0; y < GameMap.SIZE_OF_MAP_Y; y++) {
            for (int x = 0; x < GameMap.SIZE_OF_MAP_X; x++) {
                objPanel[x][y].changeState(GameMap.getVision(new Coordinates(x, y)));
            }
        }
        chPanel.repaint();
    }
}
