package pvz.rendering;

import pvz.GameMap;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControlPanel extends JPanel {

    public ControlPanel(){
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setLayout(new FlowLayout());

        JButton play = new JButton("Play");

        play.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GameMap.play();
            }
        });

        JButton stop = new JButton("Stop");
        stop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GameMap.stop();
            }
        });

        JButton change = new JButton("Get Graphic");
        change.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        GraphFrame frame = new GraphFrame();
                    }
                });
            }
        });

        add(play);
        add(stop);
        add(change);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(1100,50);
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
    }
}
