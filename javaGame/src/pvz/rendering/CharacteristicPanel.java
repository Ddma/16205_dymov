package pvz.rendering;

import pvz.GameMap;
import pvz.gameObjects.GameObject;
import pvz.gameObjects.GamePlayer;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class CharacteristicPanel extends JPanel {

    private int previousCycleTurns = 0;
    private int currentCycle = 0;
    private int currentCycleTurns = 0;
    private int maxTurnsAlive = 0;

    private static int botsDeadFromTiredness = 0;
    private static int foodTransformated = 0;
    private static int foodEaten = 0;
    private static int poisonEaten = 0;

    public CharacteristicPanel(){
        setBorder(BorderFactory.createLineBorder(Color.BLACK));

 //       setLayout(new GridLayout(0,1));
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        List<GamePlayer> bots = GameMap.getListOfBots();
        JPanel turns = new JPanel(){
            @Override
            public Dimension getPreferredSize() { return new Dimension(200,40); }
            @Override
            public void paintComponent(Graphics g) {
                g.drawString("Turns passed: " + Integer.toString(GameMap.getTurnsFromStart()), 10, 20);
            }
        };
        add(turns);
        JPanel cycles = new JPanel(){
            @Override
            public Dimension getPreferredSize() { return new Dimension(200,40); }
            @Override
            public void paintComponent(Graphics g){
                g.drawString("Cycles passed: " + Integer.toString(GameMap.getTimesUsedUpdate()),10,20);
            }
        };
        add(cycles);
        JPanel turnsPerCycle = new JPanel(){
            @Override
            public Dimension getPreferredSize() { return new Dimension(200,80); }
            @Override
            public void paintComponent(Graphics g){
                currentCycleTurns++;
                if(currentCycle != GameMap.getTimesUsedUpdate()){
                    previousCycleTurns = GameMap.getTurnsFromStart() - previousCycleTurns;
                    currentCycle++;
                    previousCycleTurns = currentCycleTurns;
                    currentCycleTurns = 0;
                }
                g.drawString("Turns",10,20);
                g.drawString("Cylce ago: " + Integer.toString(previousCycleTurns),10,40);
                g.drawString("Now: " + Integer.toString(currentCycleTurns),10,60);
            }
        };
        add(turnsPerCycle);

        JPanel interestingFeatures = new JPanel(){
            @Override
            public Dimension getPreferredSize() { return new Dimension(200,200); }
            @Override
            public void paintComponent(Graphics g){
                int ref=0;
                for(GamePlayer i:bots) {
                    if (i.getTurnsAlive() > ref)
                        ref = i.getTurnsAlive();
                }
                if(maxTurnsAlive < ref)
                    maxTurnsAlive =ref;
                g.drawString("Max turns alive for 1 bot",10,20);
                g.drawString("Overall: " + Integer.toString(maxTurnsAlive),10,40);
                g.drawString("Now: " + Integer.toString(ref),10,60);
                g.drawString("Now",10,100);
                g.drawString("Bots: " + Integer.toString(GameMap.getListOfBots().size()),10,120);
                g.drawString("Food: " + Integer.toString(GameMap.getListOfFood().size()),10,140);
                g.drawString("Poison: " + Integer.toString(GameMap.getListOfPoison().size()),10,160);
            }
        };
        add(interestingFeatures);

        JPanel botsDied = new JPanel(){
            @Override
            public Dimension getPreferredSize() { return new Dimension(200,80); }
            @Override
            public void paintComponent(Graphics g){
                g.drawString("Bots died ",10,20);
                g.drawString("From lack of energy: " + Integer.toString(botsDeadFromTiredness),10,40);
                g.drawString("From poison: " + Integer.toString(poisonEaten),10,60);
            }
        };
        add(botsDied);

        JPanel food = new JPanel(){
            @Override
            public Dimension getPreferredSize() { return new Dimension(200,80); }
            @Override
            public void paintComponent(Graphics g){
                g.drawString("Food ",10,20);
                g.drawString("Food eaten: " + Integer.toString(foodEaten),10,40);
                g.drawString("Food transformated : " + Integer.toString(foodTransformated),10,60);
            }
        };
        add(food);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(200,900);
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
    }

    public static void botDeadFromTiredness(){
        botsDeadFromTiredness++;
    }
    public static void foodTransformated(){
        foodTransformated++;
    }
    public static void foodEaten(){
        foodEaten++;
    }
    public static void poisonEaten(){
        poisonEaten++;
    }
}
