package pvz.rendering;

import javax.swing.*;

public class GraphFrame extends JFrame {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 400;

    private static GraphComponent graph = new GraphComponent();

    public GraphFrame() {
        super("Graphic");
        setSize(WIDTH, HEIGHT);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        add(graph);


        setVisible(true);
    }

    public static void updateGraphic(){
        if(graph!=null)
            graph.repaint();
    }
}
