package pvz.commands;


import java.util.Arrays;
import java.util.Iterator;

public class CommandCollection {
    private Command[] commands;
    private int currentPointercommand = 0;
    private static final int maxNumOfCommands = 20;

    public CommandCollection(){
        commands = new Command[maxNumOfCommands];
        for(int i=0;i<commands.length;i++){
            commands[i] = Command.getRandomCommand();
        }
    }

    public CommandCollection(CommandCollection parent,boolean mutation){
        commands = Arrays.copyOf(parent.getArrayOfComands(),parent.getArrayOfComands().length);
        if (mutation){
            commands[(int)(Math.random()*maxNumOfCommands)] = Command.getRandomCommand();
        }
    }

    public Command currentCommand(){
        return commands[currentPointercommand];
    }
    public void movePointerTo(int num){
        currentPointercommand += num;
        currentPointercommand %= maxNumOfCommands;
    }

    public Command[] getArrayOfComands(){
        return commands;
    }
}
