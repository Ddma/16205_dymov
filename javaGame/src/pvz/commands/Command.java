package pvz.commands;

import pvz.enumerations.Direction;
import pvz.gameObjects.*;

public abstract class Command {
    private final Direction direction;

    protected Command (Direction direction){
        this.direction = direction;
    }

    public static Command getRandomCommand() {
        Direction direction = Direction.getRandomDirection();
        double random = Math.random();
        if (random < (double)1/3) {
            return new CommandMove(direction);
        } else if (random < (double)2/3)
            return new CommandTakeFood(direction);
        else {
            return new CommandTurnPoisonToFood(direction);
        }
    }

    public Direction getDirection() {
        return direction;
    }

    public int getPointerchange(GameObject object){
        if(object instanceof Food)
            return 1;
        else if (object instanceof Poison)
            return 2;
        else if (object instanceof Wall)
            return 3;
        else if (object instanceof GamePlayer)
            return 4;
        else
            return 5;
    }
}
